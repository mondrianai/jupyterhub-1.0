// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.

require(["jquery", "bootstrap", "moment", "jhapi", "utils", "sweetalert2"], function(
  $,
  bs,
  moment,
  JHAPI,
  utils,
  Swal
) {
  "use strict";

  var base_url = window.jhdata.base_url;
  var prefix = window.jhdata.prefix;
  var admin_access = window.jhdata.admin_access;
  var options_form = window.jhdata.options_form;

  var api = new JHAPI(base_url);

  function getRow(element) {
    var original = element;
    while (!element.hasClass("server-row")) {
      element = element.parent();
      if (element[0].tagName === "BODY") {
        console.error("Couldn't find row for", original);
        throw new Error("No server-row found");
      }
    }
    return element;
  }

  function resort(col, order) {
    var query = window.location.search.slice(1).split("&");
    // if col already present in args, remove it
    var i = 0;
    while (i < query.length) {
      if (query[i] === "sort=" + col) {
        query.splice(i, 1);
        if (query[i] && query[i].substr(0, 6) === "order=") {
          query.splice(i, 1);
        }
      } else {
        i += 1;
      }
    }
    // add new order to the front
    if (order) {
      query.unshift("order=" + order);
    }
    query.unshift("sort=" + col);
    // reload page with new order
    window.location = window.location.pathname + "?" + query.join("&");
  }

  $("th").map(function(i, th) {
    th = $(th);
    var col = th.data("sort");
    if (!col || col.length === 0) {
      return;
    }
    var order = th.find("i").hasClass("fa-sort-desc") ? "asc" : "desc";
    th.find("a").click(function() {
      resort(col, order);
    });
  });

  $(".time-col").map(function(i, el) {
    // convert ISO datestamps to nice momentjs ones
    el = $(el);
    var m = moment(new Date(el.text().trim()));
    el.text(m.isValid() ? m.fromNow() : "Never");
  });

  $(".stop-server").click(function() {
    var el = $(this);
    var row = getRow(el);
    var serverName = row.data("server-name");
    var user = row.data("user");
//    el.text("stopping...");
    el.addClass("d-none");
    row.find("#server-stopping").removeClass('d-none');
    row.find(".fa-circle").removeClass('text-success').addClass('text-info');
    var stop = function(options) {
      return api.stop_server(user, options);
    };
    if (serverName !== "") {
      stop = function(options) {
        return api.stop_named_server(user, serverName, options);
      };
    }
    stop({
      success: function() {
//        el.text("stop " + serverName).addClass("d-none");
        row.find("#server-stopping").addClass('d-none');
        row.find(".stop-server").addClass("d-none");
        row.find(".access-server").addClass("d-none");
        row.find(".start-server").removeClass("d-none");
        row.find(".fa-circle").removeClass('text-info').addClass('text-danger');
      },
    });
  });

  $(".delete-server").click(function() {
    var el = $(this);
    var row = getRow(el);
    var serverName = row.data("server-name");
    var user = row.data("user");
    el.text("deleting...");
    api.delete_named_server(user, serverName, {
      success: function() {
        row.remove();
      },
    });
  });

  $(".access-server").map(function(i, el) {
    el = $(el);
    var row = getRow(el);
    var user = row.data("user");
    var serverName = row.data("server-name");
    el.attr(
      "href",
      utils.url_path_join(prefix, "user", user, serverName, "lab?") + "/"
    );
  });

  if (admin_access && options_form) {
    // if admin access and options form are enabled
    // link to spawn page instead of making API requests
    $(".start-server").map(function(i, el) {
      el = $(el);
      var row = getRow(el);
      var user = row.data("user");
      var serverName = row.data("server-name");
      el.attr(
        "href",
        utils.url_path_join(prefix, "hub/spawn", user, serverName)
      );
    });
    // cannot start all servers in this case
    // since it would mean opening a bunch of tabs
    $("#start-all-servers").addClass("d-none");
  } else {
    $(".start-server").click(function() {
      var el = $(this);
      var row = getRow(el);
      var user = row.data("user");
      var serverName = row.data("server-name");
      el.text("starting...");
      var start = function(options) {
        return api.start_server(user, options);
      };
      if (serverName !== "") {
        start = function(options) {
          return api.start_named_server(user, serverName, options);
        };
      }
      start({
        success: function() {
          el.text("start " + serverName).addClass("d-none");
          row.find(".stop-server").removeClass("d-none");
          row.find(".access-server").removeClass("d-none");
        },
      });
    });
  }

  $("#stop-all-servers").click(function() {
    // $("#stop-all-servers-dialog").modal();
    Swal.fire({
      title: 'Stop All Servers',
      text: "Are you sure you want to stop all your users' servers? Kernels will be shutdown and unsaved data may be lost.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '$success',
      cancelButtonColor: '$danger',
      confirmButtonText: 'Stop All'
    }).then((result) => {
      if (result.value) {
        $('.stop-server').not('.d-none').click();
      }
    })

  });

  $("#start-all-servers").click(function() {
    // $("#start-all-servers-dialog").modal();
    Swal.fire({
      title: 'Start All Servers',
      text: "Are you sure you want to start all servers? This can slam your server resources.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '$success',
      cancelButtonColor: '$danger',
      confirmButtonText: 'Start All'
    }).then((result) => {
      if (result.value) {
        $('.start-server').not('.d-none').each(function(i){
          setTimeout(start(this), i * 500);
        });
      }
    })

  });

  $("#stop-all-servers-dialog")
    .find(".stop-all-button")
    .click(function() {
      // stop all clicks all the active stop buttons
      $(".stop-server")
        .not(".d-none")
        .click();
    });

  function start(el) {
    return function() {
      $(el).click();
    };
  }

  setInterval(function() {
    window.location.replace('/admin');
  }, 1000 * 20);

  $("#start-all-servers-dialog")
    .find(".start-all-button")
    .click(function() {
      $(".start-server")
        .not(".d-none")
        .each(function(i) {
          setTimeout(start(this), i * 500);
        });
    });

});
