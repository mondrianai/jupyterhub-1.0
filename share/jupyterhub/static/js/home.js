// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.

require(["jquery", "moment", "jhapi", "utils", "sweetalert2"], function(
  $,
  moment,
  JHAPI,
  utils,
  Swal
) {
  "use strict";

  var base_url = window.jhdata.base_url;
  var prefix = window.jhdata.prefix;
  var user = window.jhdata.user;
  var api = new JHAPI(base_url);

  // Named servers buttons

  function getRow(element) {
    while (!element.hasClass("home-server-row")) {
      element = element.parent();
    }
    return element;
  }

  function disableRow(row) {
    row
      .find(".btn")
      .attr("disabled", true)
      .off("click");
  }

  function enableRow(row, running) {
    // enable buttons on a server row
    // once the server is running or not
    row.find(".btn").attr("disabled", false);
    row.find(".stop-server").click(stopServer);
    row.find(".delete-server").click(deleteServer);

    if (running) {
      row.find(".start-server").addClass("hidden");
      row.find(".delete-server").addClass("hidden");
      row.find(".stop-server").removeClass("hidden");
      row.find(".server-link").removeClass("hidden");
    } else {
      row.find(".start-server").removeClass("hidden");
      row.find(".delete-server").removeClass("hidden");
      row.find(".stop-server").addClass("hidden");
      row.find(".server-link").addClass("hidden");
    }
  }

  function stopServer() {
    var row = getRow($(this));
    var serverName = row.data("server-name");

    // before request
    disableRow(row);

    // request
    api.stop_named_server(user, serverName, {
      success: function() {
        enableRow(row, false);
      },
    });
  }

  function deleteServer() {
    var row = getRow($(this));
    var serverName = row.data("server-name");

    // before request
    disableRow(row);

    // request
    api.delete_named_server(user, serverName, {
      success: function() {
        row.remove();
      },
    });
  }

  // initial state: hook up click events
  $("#stop").click(function() {

   // stopping
    $("default-server-stopping").removeClass('d-none');
    $("default-server-circle").removeClass('text-success').addClass('text-info');
    // access
    $("access-server").addClass('d-none');
    // request
    api.stop_server(user, {
      success: function() {
        $("default-server-stopping").addClass('d-none');
        $("default-server-circle").removeClass('text-info').addClass('text-danger');
        // stop
        $("stop").addClass('d-none');
        // start
        $('start').removeClass('d-none');
      },
    });
  });

  $(".new-server-btn").click(function() {
    var row = getRow($(this));
    var serverName = row.find(".new-server-name").val();
    if (serverName === "") {
      // ../spawn/user/ causes a 404, ../spawn/user redirects correctly to the default server
      window.location.href = "./spawn/" + user;
    } else {
      window.location.href = "./spawn/" + user + "/" + serverName;
    }
  });

  $(".stop-server").click(stopServer);
  $(".delete-server").click(deleteServer);


  $(".stop-server-custom").click(function() {
    const serverName = this.id.replace('stop-','');
    // stopping
    $(`#stopping-${serverName}`).removeClass('d-none');
    $(`#server-circle-${serverName}`).removeClass('text-success').addClass('text-info');
    // access
    $(`#access-${serverName}`).addClass('d-none');
    // request
    api.stop_named_server(user, serverName, {
      success: function() {
        $(`#stopping-${serverName}`).addClass('d-none');
        $(`#server-circle-${serverName}`).removeClass('text-info').addClass('text-danger');
        // stop
        $(`#stop-${serverName}`).addClass('d-none');
        // start
        $(`#start-${serverName}`).removeClass('d-none');
        // stop
        $(`#delete-${serverName}`).removeClass('d-none');
      },
    });
  });

  $(".delete-server-custom").click(function() {
    const serverName = this.id.replace('delete-','');
    // request
    api.delete_named_server(user, serverName, {
      success: function() {
        // row.remove();
        $(`#col-${serverName}`).remove();
      },
    });
  });

//  $(".create-sever").click(function() {
//    Swal.fire({
//      title: 'Type the new sever name',
//      input: 'text',
//      inputAttributes: {
//        autocapitalize: 'off'
//      },
//      // html:
//      //   '<input id="swal-input1" class="swal2-input">' +
//      //   '<input id="swal-input2" class="swal2-input">',
//      showCancelButton: true,
//      confirmButtonText: 'Create',
//      showLoaderOnConfirm: true,
//      allowOutsideClick: () => !Swal.isLoading()
//    }).then((result) => {
//      if (result.value) {
//        // console.log(`result.value:${result.value}`);
//        window.location.href = "./spawn/" + user + "/" + result.value;
//      }
//    })
//  });

  // render timestamps
  $(".time-col").map(function(i, el) {
    // convert ISO datestamps to nice momentjs ones
    el = $(el);
    var m = moment(new Date(el.text().trim()));
    el.text(m.isValid() ? m.fromNow() : "Never");
  });
});
