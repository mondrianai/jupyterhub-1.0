// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.

require(["jquery", "jhapi", "moment"], function($, JHAPI, moment) {
  "use strict";

  var base_url = window.jhdata.base_url;
  var user = window.jhdata.user;
  var api = new JHAPI(base_url);

  $(".project-name").on('input', function() {
    const value =  $(this).val();

    // const allowedChars = new RegExp("^[a-zA-Z0-9\-]+$");
    const forbiddenChars = new RegExp("[^a-zA-Z0-9\-]", 'g');
    const convertedValue = value.replace(forbiddenChars, '').toLowerCase();
    $(".project-slug").val(convertedValue);
  });

});
