import pytz
from datetime import datetime

from . import orm
from . import ormnative
from .utils import maybe_future, url_path_join

def tryGettingKey(dict, key):
    try:
        return dict[key]
    except KeyError:
        return None

def convertFloat(str, places, multiple):
    return format(float(str)*multiple, '.'+places+'f')

def formatBytes(sizeStr):
    size = int(sizeStr)
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0 : '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    return format(size, '.2f')+power_labels[n]+'B'

#  Time

def getCurrentTimeKoreaSeoul():
    #     fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    fmt = "%Y-%m-%d %H:%M:%S"
    return datetime.now(pytz.timezone('Asia/Seoul')).strftime(fmt)

def convertDateTimeToKoreaSeoul(utc_time):
    #     fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    fmt = "%Y-%m-%d %H:%M:%S"
    return pytz.utc.localize(utc_time, is_dst=None).astimezone(pytz.timezone('Asia/Seoul')).strftime(fmt)

# NativeAuthenticator

def getUserInformation(self, name):
#     return NativeAuthenticator.get_user_information(self, name)
#     return None
    return ormnative.UserInfo.find(self.db, name)


def changeUseInformation(self, name, realName, email, phone, organization):
    #     NativeAuthenticator.change_profile(self, name, realName, email, phone, organization)
    user = getUserInformation(self, name)
    user.user_real_name = realName
    user.email = email
    user.phone = phone
    user.organization = organization
    self.db.commit()

async def create_user_from_nativeauthenticator(self, username, pw, user_real_name, email, phone, organization, has_2fa):
    """Login a user"""
    print('create_user_from_nativeauthenticator()')
    username = username
    auth_state = None
    admin = 0
    new_user = username not in self.users
    user = self.user_from_username(username)
    await maybe_future(self.authenticator.add_user(user))
    self.db.commit()

async def set_jupyterhub_user_admin(self, username):
    user = orm.User.find(self.db, username)
    user.admin = not user.admin
    self.db.commit()

def get_jupyterhub_users(self):
    available = {'name', 'admin', 'running', 'last_activity'}
    default_sort = ['admin', 'name']
    mapping = {
        'running': orm.Spawner.server_id,
    }
    for name in available:
        if name not in mapping:
            mapping[name] = getattr(orm.User, name)

    default_order = {
        'name': 'asc',
        'last_activity': 'desc',
        'admin': 'desc',
        'running': 'desc',
    }

    sorts = self.get_arguments('sort') or default_sort
    orders = self.get_arguments('order')

    for bad in set(sorts).difference(available):
        self.log.warning("ignoring invalid sort: %r", bad)
        sorts.remove(bad)
    for bad in set(orders).difference({'asc', 'desc'}):
        self.log.warning("ignoring invalid order: %r", bad)
        orders.remove(bad)

    # add default sort as secondary
    for s in default_sort:
        if s not in sorts:
            sorts.append(s)
    if len(orders) < len(sorts):
        for col in sorts[len(orders):]:
            orders.append(default_order[col])
    else:
        orders = orders[:len(sorts)]

    # this could be one incomprehensible nested list comprehension
    # get User columns
    cols = [ mapping[c] for c in sorts ]
    # get User.col.desc() order objects
    ordered = [ getattr(c, o)() for c, o in zip(cols, orders) ]

    users = self.db.query(orm.User).outerjoin(orm.Spawner).order_by(*ordered)
    users = [ self._user_from_orm(u) for u in users ]
    return users

def add_server_information(self, user, serverName, serverSlug, serverDescription, serverPrivacy):
    """Add a sever information to the database."""
    server_information = orm.ServerInformation(name=serverName, user_id=user.id, slug=serverSlug, description=serverDescription, privacy=serverPrivacy)
    self.db.add(server_information)
    self.db.commit()

    url = "/spawn/" + user.name + "/" + serverSlug
    self.redirect(url)

def get_server_information_by_user(self, userID):
    return orm.ServerInformation.find_by_user(self.db, userID)

def get_server_information_by_slug(db, slug):
    return orm.ServerInformation.find_by_slug(db, slug)
